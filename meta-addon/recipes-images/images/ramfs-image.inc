# methods to create fitImage with ramdisk
# some parts copied from kernel-fitimage.bbclass
# it is not build in the context of the kernel, it can be used for "any small" images
# the size of the decopressed file system must be not greater than specified CONFIG_BLK_DEV_RAM_SIZE
#
# The following variables must be defined
# typically in machine.conf
# ${KERNEL_IMAGETYPE}
# ${UBOOT_ARCH}
# ${UBOOT_LOADADDRESS}, ${UBOOT_ENTRYPOINT}
# ${UBOOT_RD_LOADADDRESS}, ${UBOOT_RD_ENTRYPOINT}
# ${KERNEL_DEVICETREE}
#
# from image
# ${IMAGE_BASENAME}, ${IMAGE_NAME}
# ${IMAGE_FSTYPES} set only to one type

DEPENDS += " \
	   dtc-native \
	   u-boot-mkimage-native \
"

FITIMAGE_ITS_FILE = "${IMAGE_NAME}.its"
FITIMAGE_ITB_FILE = "${IMAGE_NAME}.itb"
FITIMAGE_RAMDISK_FILE = "${IMAGE_NAME}.rootfs.${IMAGE_FSTYPES}"

fitimage_emit_fit_header() {
	cat << EOF >> ${1}
/dts-v1/;

/ {
	description = "U-Boot fitImage for ${DISTRO_NAME}/${IMAGE_NAME}/${MACHINE}";
	#address-cells = <1>;
EOF
}

fitimage_emit_section_imagestart() {
		cat << EOF >> ${1}
	images {
EOF
}
fitimage_emit_section_confstart() {
		cat << EOF >> ${1}
	configurations {
EOF
}
fitimage_emit_section_sectend() {
		cat << EOF >> ${1}
	};
EOF
}
fitimage_emit_section_fitend() {
		cat << EOF >> ${1}
};
EOF
}

fitimage_emit_section_kernel() {
	cat << EOF >> ${1}
		kernel@1 {
			description = "Linux kernel";
			data = /incbin/("${KERNEL_IMAGETYPE}");
			type = "kernel";
			arch = "${UBOOT_ARCH}";
			os = "linux";
			compression = "none";
			load = <${UBOOT_LOADADDRESS}>;
			entry = <${UBOOT_ENTRYPOINT}>;
			hash@1 {
				algo = "sha1";
			};
		};
EOF
}

fitimage_emit_section_dtb() {
	cat << EOF >> ${1}
		fdt@${2} {
			description = "Flattened Device Tree blob";
			data = /incbin/("${2}");
			type = "flat_dt";
			arch = "${UBOOT_ARCH}";
			compression = "none";
			hash@1 {
				algo = "sha1";
			};
		};
EOF
}

fitimage_emit_section_ramdisk() {
	ramdisk_ctype="none"

	case $2 in
		*.gz)
			ramdisk_ctype="gzip"
			;;
		*.bz2)
			ramdisk_ctype="bzip2"
			;;
		*.lzma)
			ramdisk_ctype="lzma"
			;;
		*.lzo)
			ramdisk_ctype="lzo"
			;;
		*.lz4)
			ramdisk_ctype="lz4"
			;;
	esac

	cat << EOF >> ${1}
		ramdisk@1 {
			description = "${2}";
			data = /incbin/("${2}");
			type = "ramdisk";
			arch = "${UBOOT_ARCH}";
			os = "linux";
			compression = "${ramdisk_ctype}";
			load = <${UBOOT_RD_LOADADDRESS}>;
			entry = <${UBOOT_RD_ENTRYPOINT}>;
			hash@1 {
				algo = "sha1";
			};
		};
EOF
}

fitimage_emit_section_config() {
	cat << EOF >> ${1}
		conf@${2} {
			description = "Linux kernel, FDT blob, ramdisk";
			kernel = "kernel@1";
			fdt = "fdt@${2}";
			ramdisk = "ramdisk@1";
			hash@1 {
				algo = "sha1";
			};
		};
EOF
}


do_assemble_fit() {
	# copy kernel, dtbs and rootfs to B, this allows to have relative path in fitImage
	cp ${DEPLOY_DIR_IMAGE}/${KERNEL_IMAGETYPE} ${B}
	cp ${DEPLOY_DIR_IMAGE}/*.dtb ${B}
	cp ${WORKDIR}/deploy-${IMAGE_BASENAME}-image-complete/${IMAGE_NAME}.rootfs.${IMAGE_FSTYPES} ${B}

	cd ${B}
	rm -f ${FITIMAGE_ITS_FILE}
	fitimage_emit_fit_header ${FITIMAGE_ITS_FILE}
	fitimage_emit_section_imagestart ${FITIMAGE_ITS_FILE}
	fitimage_emit_section_kernel ${FITIMAGE_ITS_FILE}
	for DTB in ${KERNEL_DEVICETREE}; do
		fitimage_emit_section_dtb ${FITIMAGE_ITS_FILE} ${DTB}
	done
	fitimage_emit_section_ramdisk ${FITIMAGE_ITS_FILE} ${FITIMAGE_RAMDISK_FILE}
	fitimage_emit_section_sectend ${FITIMAGE_ITS_FILE}
	fitimage_emit_section_confstart ${FITIMAGE_ITS_FILE}
	for DTB in ${KERNEL_DEVICETREE}; do
		fitimage_emit_section_config ${FITIMAGE_ITS_FILE} ${DTB}
	done
	fitimage_emit_section_sectend ${FITIMAGE_ITS_FILE}
	fitimage_emit_section_fitend ${FITIMAGE_ITS_FILE}

	uboot-mkimage -f ${FITIMAGE_ITS_FILE} ${FITIMAGE_ITB_FILE}

	# copy result files into deploy
	cp ${FITIMAGE_ITS_FILE} ${DEPLOY_DIR_IMAGE}
	cp ${FITIMAGE_ITB_FILE} ${DEPLOY_DIR_IMAGE}
}

IMAGE_POSTPROCESS_COMMAND += "do_assemble_fit; "

