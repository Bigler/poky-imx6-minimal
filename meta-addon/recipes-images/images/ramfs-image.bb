SUMMARY = "Simple RAM Image for colibri-imx6dl"

export IMAGE_BASENAME = "ramfs-image"
IMAGE_NAME = "${MACHINE}_${IMAGE_BASENAME}"

IMAGE_FSTYPES = "ext4.gz"

require recipes-images/images/ramfs-image.inc

IMAGE_INSTALL = " \
    packagegroup-core-boot \
"

EXTRA_IMAGE_FEATURES_append = " debug-tweaks"

inherit core-image

