FILESEXTRAPATHS_append := "${THISDIR}/${PN}-${PV}:"
SECTION = "kernel"
DESCRIPTION = "Mainline Linux kernel"
LICENSE = "GPLv2"

inherit kernel kernel-fitimage

LIC_FILES_CHKSUM = "file://COPYING;md5=bbea815ee2795b2f4230826c0c6b8814"

SRC_URI = "git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux.git;protocol=https;branch=linux-5.4.y \
  file://0001-defconfig-increase-size.patch \
  file://0002-defconfig-make-kernel-smaller.patch \
"

SRCREV = "31cdcb6d430f07760dd2f540a354b11e6bb6a4a4"
LINUX_VERSION ?= "5.4.151"

PV = "${LINUX_VERSION}+git${SRCPV}"

KBUILD_DEFCONFIG_colibri-imx6 = "imx_v6_v7_defconfig"

LINUX_KERNEL_TYPE = "mainline"
LINUX_VERSION_EXTENSION = "-${LINUX_KERNEL_TYPE}"

S = "${WORKDIR}/git"

DEPENDS += " lzop-native "

# mechanism to have git hash as local version
# copied from meta-freescale/classes/fsl-kernel-localversion.bbclass

SCMVERSION ??= "y"
LOCALVERSION ??= ""

kernel_conf_variable() {
       CONF_SED_SCRIPT="$CONF_SED_SCRIPT /CONFIG_$1[ =]/d;"
       if test "$2" = "n"
       then
               echo "# CONFIG_$1 is not set" >> ${B}/.config
       else
               echo "CONFIG_$1=$2" >> ${B}/.config
       fi
}

do_preconfigure() {
       mkdir -p ${B}
       echo "" > ${B}/.config
       CONF_SED_SCRIPT=""

       kernel_conf_variable LOCALVERSION "\"${LOCALVERSION}\""
       kernel_conf_variable LOCALVERSION_AUTO y

       sed -e "${CONF_SED_SCRIPT}" < '${S}/arch/${ARCH}/configs/${KBUILD_DEFCONFIG}' >> '${B}/.config'

       if [ "${SCMVERSION}" = "y" ]; then
               # Add GIT revision to the local version
               head=`git --git-dir=${S}/.git rev-parse --verify --short HEAD 2> /dev/null`
               printf "%s%s" + $head > ${S}/.scmversion
       fi
}
addtask preconfigure before do_configure after do_unpack do_patch





